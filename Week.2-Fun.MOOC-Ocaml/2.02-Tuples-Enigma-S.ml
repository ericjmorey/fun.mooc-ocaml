(*
Solution to the Tuples-Enigma exercise

Limited to 2 digit numbers

Additional challenge: 3 digit numbers and numbers of arbitrary length
*)

(* Write a function exchange of type int -> int that takes an integer x between 10 and 99 and returns an integer which is x whose digits have been exchanged. For instance, exchange 73 = 37. *)
(* int -> int *)
let exchange k =
  ((k mod 10) * 10) + (k / 10);;

exchange 72;; (* 72 *)

(* int -> bool *)
let compare_4x (x, y) =
  x = (y * 4);;

compare_4x (72, 18);; (* True *)

(* int -> bool *)
let compare_3x (x, y) =
  exchange y = ((exchange x) * 3);;

compare_3x (72, 18);; (* True *)

(* Define is_valid_answer of type int * int -> bool such that is_valid_answer (grand_father_age, grand_son_age) returns true if and only if grand_father_age and grand_son_age verify the constraints of the puzzle. *)
(* int * int -> bool *)
let is_valid_answer (grand_father_age, grand_son_age) =
  compare_4x (grand_father_age, grand_son_age)
  && 
  compare_3x (grand_father_age, grand_son_age);; 

is_valid_answer (72, 18);; (* True *)

(* Write a function find : (int * int) -> (int * int) that takes a pair (max_grand_father_age, min_grand_son_age) and returns a solution (grand_father_age, grand_son_age) to the problem, where min_grand_son_age <= grand_son_age < grand_father_age <= max_grand_father_age or (-1, -1) if there was no valid answer in the given range. *)
(* (int * int) -> (int * int) *)  
let rec find_answer (max_grand_father_age, min_grand_son_age) =
  if max_grand_father_age mod 4 = 0 then 
    if max_grand_father_age / 4 < min_grand_son_age then
      (-1, -1)
    else if is_valid_answer (max_grand_father_age, (max_grand_father_age / 4)) then
      (max_grand_father_age, (max_grand_father_age / 4))
    else find_answer ((max_grand_father_age -4), min_grand_son_age)
  else find_answer ((max_grand_father_age -1), min_grand_son_age);;
  
find_answer (72, 18);; (* (72, 18) *)
find_answer (71, 1);;  (* (-1, -1) *)
find_answer (76, 18);; (* (72, 18) *)
find_answer (72, 19);; (* (-1, -1) *)
find_answer (99, 1);;  (* (72, 18) *)

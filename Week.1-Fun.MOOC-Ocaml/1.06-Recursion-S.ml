let rec gcd n m =
  if n = 0 then m
  else if m = 0 then n
  else if n > m then gcd m (n mod m)
  else gcd n (m mod n);;

gcd 0 1;;
gcd 2 0;;
gcd 3 4;;
gcd 6 5;;
gcd 9 81;;
gcd 18 24;;
gcd 270 192;;

let rec multiple_upto n r =
  if r < 2 then false
      
  else multiple_of n 2 || 
  
  
       multiple_of n r || 
       
       if (r-1) >= 3 then multiple_upto n (r - 1)
       else false;;

(* need to account  for In other words that there exists a number 
   d >= 2 and <= r, such that the remainder of the division of n by d is zero.*)

multiple_upto 3 1;;
multiple_upto 10 9;;
multiple_upto 11 9;;

let rec is_prime n = 
  if n <= 1 then false
  else if n = 2 then true
  else  not (multiple_upto n (n-1));;

is_prime 1;;
is_prime 2;;
is_prime 3;;
is_prime 4;;
is_prime 5;;
  
multiple_upto 1 1;;
multiple_upto 2 2;;
multiple_upto 2 1;;
multiple_upto 3 2;;
multiple_upto 4 3;;
multiple_upto 5 4;;
multiple_upto 6 5;;
multiple_upto 7 6;;
multiple_upto 8 7;;
multiple_upto 9 8;;
multiple_upto 10 9;;
multiple_upto 11 10;;


(* multiple_of that takes two integer parameters, n and d, and determines 
whether n is a multiple of d. The function must return a boolean value. This 
      function can be written without recursion. Look at the operators defined on 
    integers in sequence 1. *)

(* integer_square_root that calculates the integer square root of a positive 
integer n, that is the largest integer r such that r * r <= n. Hint: you may 
               use floating point arithmetic, but don't forget that you have to convert 
    explicitely between float and int. *)